import React, { Component } from 'react'
import CardShoes from './CardShoes'

export default class ListShoes extends Component {

    // viet ham render list shoes
    renderShoes = () => {
        let {data} = this.props;
        // console.log('data: ', data);
        return data.map((item, index)=>{
            // console.log('item: ', item);
            return (
                <CardShoes handleCart={this.props.handleCart} handleDetail={this.props.handleDetail} key={index} cardShoes = {item}/>
            )
        })
    }
    render() {
    return (
      <div className='row mx-auto'>
        {this.renderShoes()}
      </div>
    )
  }
}
