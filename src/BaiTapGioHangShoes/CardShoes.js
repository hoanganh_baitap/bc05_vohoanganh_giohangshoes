import React, { Component } from "react";

export default class CardShoes extends Component {
  render() {
    // console.log(this.props);
    let {cardShoes} = this.props;
    return (
      <div className="col-md-3 p-2">
        {/* tao card chua noi dung san pham */}
        <div className="card" style={{border: "none"}}>
          <img className="card-img-top" src={cardShoes.image} style={{width: 250}} alt ="" />
          <div className="card-body">
            <h6 className="card-title">{cardShoes.name}</h6>
            <button onClick={()=>this.props.handleCart(cardShoes)} className="btn btn-warning mr-1">Buy</button>
            <button onClick={()=>this.props.handleDetail(cardShoes)} className="btn btn-success">Detail</button>
          </div>
        </div>
      </div>
    );
  }
}
