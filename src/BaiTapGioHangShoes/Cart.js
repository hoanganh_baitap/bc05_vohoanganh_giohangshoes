import React, { Component } from 'react'

export default class Cart extends Component {
    // viet ham render san pham khi nguoi dung click buy
    renderCart = () =>{
        let {cart} = this.props;
        return cart.map((item, index)=>{
            return (
                <tr key={index} style={{lineHeight: 5.8}}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td><img src={item.image} style={{width: 100}} alt="" /></td>

                </tr>
            )
        })
    }
  render() {
    // console.log(this.props);
    return (
      <table className='table text-center text-light bg-warning'>
        <thead>
            <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>PRICE</th>
                <th>IMAGE</th>
            </tr>
        </thead>
        <tbody>
            {this.renderCart()}
        </tbody>
      </table>
    )
  }
}
