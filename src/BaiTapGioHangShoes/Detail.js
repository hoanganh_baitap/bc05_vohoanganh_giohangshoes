import React, { Component } from 'react'

export default class Detail extends Component {
  render() {
    let {detailShoes} = this.props;
    return (
      <div className='row bg-success text-light'>
        <div className='col-6'>
            <img src={detailShoes.image} alt="" />
        </div>
        <div className='col-6 p-5' style={{fontSize: "20px"}}>
            <p>{detailShoes.name}</p>
            <p>{detailShoes.alias}</p>
            <p>{detailShoes.price}</p>
            <p>{detailShoes.description}</p>
            <p>{detailShoes.shortDescription}</p>
        </div>
      </div>
    )
  }
}
