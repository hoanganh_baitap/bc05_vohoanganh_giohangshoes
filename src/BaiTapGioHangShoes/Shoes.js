/**
 *      <App>
 *            <Shoes>
 *                   <Cart>                  
 *                   </Cart>   
 *              
 *                   <ListShoes>
 *                              <CardShoes>
 *                              </CardShoes>
 *                   </ListShoes>
 * 
 *                   <Detail>
 *                   </Detail>
 *            </Shoes>
 *      </App>
 */

import React, { Component } from 'react'
import Cart from './Cart'
import Detail from './Detail'
import ListShoes from './ListShoes'
import {data} from "./data"

export default class Shoes extends Component {

state = {
    dataShoes: data,
    detail: data[0],
    cart: [],
}

// viet ham khi nguoi dung click vao button buy thi se push shoes vao mang roi render ra table tao o cart
handleCart = (cart)=>{
    let cartArr = [...this.state.cart];
    cartArr.push(cart);
    // thay doi noi dung mang cart bang mang cartArr khi nguoi dung click buy
    this.setState({
        cart: cartArr,
    })
}

// viet ham khi nguoi dung click vao button detail, se thay doi hinh anh va noi dung shoes
handleDetail = (detail) => {
    this.setState({
        detail: detail,
    })
}

render() {
    return (
      <div className='container py-5'>
        <Cart cart = {this.state.cart}/>
        <h1 className='text-center pb-5'>SHOES</h1>
        <ListShoes handleCart={this.handleCart} handleDetail={this.handleDetail} data = {this.state.dataShoes}/>
        <Detail detailShoes = {this.state.detail}/>
      </div>
    )
  }
}
