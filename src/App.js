import logo from './logo.svg';
import './App.css';
import Shoes from './BaiTapGioHangShoes/Shoes';

function App() {
  return (
    <div className="App">
      <Shoes />
    </div>
  );
}

export default App;
